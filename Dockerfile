#Step 1

FROM node:17-alpine as build-step

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build --prod

#Step2

FROM nginx:alpine

COPY --from=build-step /app/dist/frontend /usr/share/nginx/html

EXPOSE 80
