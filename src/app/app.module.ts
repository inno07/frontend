//Core Modules
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
//External Modules
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from "@angular/common/http";
//Internal Modules
import { MaterialModule } from "./shared/material.module";
//Components
import { NavigationComponent } from './navigation/navigation.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ChartsModule } from "./charts/charts.module";
import { ListComponent } from './list/list.component';
import { OptionsComponent } from './options/options.component';
import { StartComponent } from './start/start.component';
import { ModelComponent } from './model/model.component';
import { CreateComponent } from './create/create.component';
import { RegressionUploadComponent } from './regression/regression-upload/regression-upload.component';
import { RegressionSelectComponent } from './regression/regression-select/regression-select.component';
import { ClassificationUploadComponent } from './classification/classification-upload/classification-upload.component';
import { ClassificationResultComponent } from './classification/classification-result/classification-result.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    PageNotFoundComponent,
    DashboardComponent,
    ListComponent,
    OptionsComponent,
    StartComponent,
    ModelComponent,
    CreateComponent,
    RegressionUploadComponent,
    RegressionSelectComponent,
    ClassificationUploadComponent,
    ClassificationResultComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    ChartsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
