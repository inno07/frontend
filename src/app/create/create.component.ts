import { Component } from '@angular/core';
import {FetchResponse} from "../responses/fetch-response";
import {GraphResponse} from "../responses/graph-response";
import {ClassificationResponse} from "../responses/classification-response";

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent {

  modelActive = true;
  regressionUploadActive = false;
  regressionSelectActive = false;
  regressionGraphActive = false;
  classificationUploadActive = false;
  classificationResultActive = false;
  hasLimit = false;

  fetchResponse: FetchResponse = { title: '', headers: []};

  graphResponse: GraphResponse = {
    line: {
      name: '',
      series: []
    },
    scatter: {
      name: '',
      series: []
    },
    info: {
      loss: 0
    }
  };

  classificationResponse: ClassificationResponse = {
    head: [],
    toPredict: '',
    data: []
  };

  constructor() { }

  request = {
    model: '',
    formData: ''
  }

  getModelOutput($event: any){
    this.request.model = $event;
    this.modelActive = false;

    if($event === "regression")
      this.regressionUploadActive = true;

    if($event === "classification")
      this.classificationUploadActive = true;
  }

  getRegressionUploadOutput($event: any){
    this.fetchResponse = $event;

    this.regressionUploadActive = false;
    this.regressionSelectActive = true;
  }

  getRegressionSelectOutput($event: any){
    this.graphResponse = $event;

    this.regressionSelectActive = false;
    this.regressionGraphActive = true;
  }

  getClassificationUploadOutput($event: any){
    this.classificationResponse = $event;

    console.log(this.classificationResponse);

    this.classificationUploadActive = false;
    this.classificationResultActive = true;
  }

}
