export interface LinePoint {
  name: number;
  value: number;
}

export interface ScatterPoint {
  name: number;
  x: number;
  y: number;
  r: number;
}

export interface GraphInfo {
  loss: number;
}

export interface GraphResponse {
  line: {
    name: string,
    series: Array<LinePoint>
  },
  scatter: {
    name: string,
    series: Array<ScatterPoint>
  },
  info: GraphInfo
}
