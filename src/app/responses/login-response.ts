export interface LoginResponse {
  message: string;
  userID: number;
  token: string;
}
