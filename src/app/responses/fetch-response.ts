export interface FetchResponse {
  title: string;
  headers: Array<string>;
}
