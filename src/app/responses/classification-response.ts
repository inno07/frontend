export interface ClassificationResponse{
  head: string[];
  toPredict: string;
  data: ClassificationResponseItem[]
}

export interface ClassificationResponseItem {
  colValues: any[];
  percentage: number;
}
