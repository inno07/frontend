import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {DataService} from "../services/data.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {FetchResponse} from "../responses/fetch-response";
import {RegressionRequest} from "../requests/regression-request";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  list: Array<FetchResponse> = [];
  active: FetchResponse | null = null;
  title: string = "Files";

  @Output() listActiveOutput = new EventEmitter<FetchResponse>();

  constructor(private dataService: DataService, private snackbar: MatSnackBar) { }

  ngOnInit(): void {
    this.fetch();
  }

  public update(entry: FetchResponse){
    this.list.push(entry);
  }

  fetch(){
    const userID = localStorage.getItem('userID');

    if(userID == null)
      return;

    this.dataService.fetch(Number(userID)).subscribe({
      next: res => {
        console.log(res);
        this.list = res;
      },
      error: err => {
        console.log(err);
        this.snackbar.open('Unexpected server error', undefined, {
          duration: 3000,
          panelClass: ['error-snackbar']
        })
      }
    })
  }

  upload(event: any) {
    if(event.target == null)
      return;

    const file: File = event.target.files[0];

    const formData = new FormData();
    formData.append("userID", "0123456789");
    formData.append("file", file);

    const uploadRequest: RegressionRequest = {
      formData: formData,
      ts: new Date()
    }

    this.dataService.regression(uploadRequest).subscribe(res => {
      console.log(res);
      this.list = this.list || [];
      this.list.push(res);
      console.log(this.list);
    });
  }

  select(entry: FetchResponse){
    console.log(entry);
    this.active = entry;
    this.listActiveOutput.emit(entry);
  }
}
