import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FetchResponse} from "../responses/fetch-response";
import {GraphResponse} from "../responses/graph-response";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {GraphRequest} from "../requests/graph-request";
import {DataService} from "../services/data.service";

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss']
})
export class OptionsComponent implements OnInit {

  title = 'Options';

  models = ["Classification", "Linear Regression"];

  @Input() listActiveInput: FetchResponse | null = { title: '', headers: []};

  @Output() graphActiveOutput = new EventEmitter<GraphResponse>();

  @Output() graphLoadingOutput = new EventEmitter<boolean>();

  graph = new FormGroup({
    model: new FormControl('', Validators.required),
    xAxis: new FormControl('', Validators.required),
    yAxis: new FormControl('', Validators.required)
  });

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
  }

  submit(){

    console.log(this.graph);

    if(this.graph.invalid || this.listActiveInput == null)
      return;

    this.load(true);

    localStorage.setItem('graphXAxis', this.graph.value.xAxis);
    localStorage.setItem('graphYAxis', this.graph.value.yAxis);

    const graphRequest: GraphRequest = {
      model: this.graph.value.model,
      data: this.listActiveInput.title,
      xAxis: this.graph.value.xAxis,
      yAxis: this.graph.value.yAxis,
      ts: new Date()
    };

    this.dataService.graph(graphRequest).subscribe({
      next: res => {
        console.log(res);
        this.display(res);
        this.load(false);
      },
      error: err => {
        console.log(err);
        this.load(false);
      }
    })
  }

  display(graph: GraphResponse){
    this.graphActiveOutput.emit(graph);
  }

  load(isLoading: boolean){
    this.graphLoadingOutput.emit(isLoading);
  }

}
