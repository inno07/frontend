import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HighchartsChartModule } from 'highcharts-angular';
import { RegressionGraphComponent } from "../regression/regression-graph/regression-graph.component";
import {MatButtonModule} from "@angular/material/button";
import {RouterModule} from "@angular/router";
import {MatIconModule} from "@angular/material/icon";
import {MatTooltipModule} from "@angular/material/tooltip";

@NgModule({
  declarations: [
    RegressionGraphComponent
  ],
    exports: [
        RegressionGraphComponent
    ],
    imports: [
        CommonModule,
        HighchartsChartModule,
        MatButtonModule,
        RouterModule,
        MatIconModule,
        MatTooltipModule
    ]
})
export class ChartsModule { }
