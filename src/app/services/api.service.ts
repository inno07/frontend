import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { RegressionRequest } from "../requests/regression-request";
import { FetchResponse } from "../responses/fetch-response";
import { GraphRequest } from "../requests/graph-request";
import { GraphResponse } from "../responses/graph-response";
import {ClassificationLimitRequest} from "../requests/classification-limit-request";
import {ClassificationNoLimitRequest} from "../requests/classification-no-limit-request";
import {ClassificationResponse} from "../responses/classification-response";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  regression(regressionRequest: RegressionRequest) {
    return this.http.post<FetchResponse>(`${environment.apiGatewayURLFlask}/upload`, regressionRequest.formData);
  }

  classificationLimit(classificationLimitRequest: ClassificationLimitRequest) {
    const url = `${environment.apiGatewayURLFlask}/classification?dependentCols=${classificationLimitRequest.dependentCols}&parameterCol=${classificationLimitRequest.predictCol}&limit=${classificationLimitRequest.limit}`;
    console.log(url)
    return this.http.post<ClassificationResponse>(url, classificationLimitRequest.formData);
  }

  classificationNoLimit(classificationNoLimitRequest: ClassificationNoLimitRequest){
    const url = `${environment.apiGatewayURLFlask}/classification?dependentCols=${classificationNoLimitRequest.dependentCols}&parameterCol=${classificationNoLimitRequest.predictCol}`;
    console.log(url)
    return this.http.post<ClassificationResponse>(url, classificationNoLimitRequest.formData)
  }

  fetch(userID: number) {
    return this.http.get<Array<FetchResponse>>(`${environment.apiGatewayURLFlask}/fetch/${userID}`);
  }

  graph(graphRequest: GraphRequest) {
    return this.http.get<GraphResponse>(`${environment.apiGatewayURLFlask}/tensorflow/0123456789/${graphRequest.data}?x=${graphRequest.xAxis}&y=${graphRequest.yAxis}`);
  }


}
