import {Injectable} from '@angular/core';
import {RegressionRequest} from "../requests/regression-request";
import {ApiService} from "./api.service";
import {GraphRequest} from "../requests/graph-request";
import {ClassificationLimitRequest} from "../requests/classification-limit-request";
import {ClassificationNoLimitRequest} from "../requests/classification-no-limit-request";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private apiService: ApiService) { }

  regression(regressionRequest: RegressionRequest){
    return this.apiService.regression(regressionRequest);
  }

  classificationLimit(classificationParamRequest: ClassificationLimitRequest){
    return this.apiService.classificationLimit(classificationParamRequest);
  }

  classificationNoLimit(classificationNoParamRequest: ClassificationNoLimitRequest){
    return this.apiService.classificationNoLimit(classificationNoParamRequest);
  }

  fetch(userID: number){
    return this.apiService.fetch(userID);
  }

  graph(graphRequest: GraphRequest){
    return this.apiService.graph(graphRequest);
  }
}
