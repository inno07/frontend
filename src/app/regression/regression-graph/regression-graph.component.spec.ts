import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegressionGraphComponent } from './regression-graph.component';

describe('RegressionGraphComponent', () => {
  let component: RegressionGraphComponent;
  let fixture: ComponentFixture<RegressionGraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegressionGraphComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegressionGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
