import {Component, HostListener, Input, OnChanges, SimpleChanges} from '@angular/core';
import {GraphResponse} from "../../responses/graph-response";
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-regression-graph',
  templateUrl: './regression-graph.component.html',
  styleUrls: ['./regression-graph.component.scss']
})
export class RegressionGraphComponent implements OnChanges {

  constructor() {
    this.onResize();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?: any) {
    this.chartOptions.chart.height = window.innerHeight * 0.7;
    this.chartOptions.chart.width = window.innerWidth * 0.8;
  }

  screenHeight: number = 0;
  screenWidth: number = 0;

  Highcharts: typeof Highcharts = Highcharts;

  data: GraphResponse = {
    line: {
      name: '',
      series: []
    },
    scatter: {
      name: '',
      series: []
    },
    info: {
      loss: 0
    }
  };

  @Input() regressionGraphInput: GraphResponse | null = {
    line: {
      name: '',
      series: []
    },
    scatter: {
      name: '',
      series: []
    },
    info: {
      loss: 0
    }
  };

  chartOptions: Highcharts.Options | any = {
    title: {
      text: '',
      style: {
        color: 'white'
      }
    },
    zoomType: 'Xy',
    chart: {
      backgroundColor: 'transparent',
      renderTo: 'container',
      width: this.screenWidth - 200,
      height: this.screenHeight - 400
    },
    xAxis: {
      title: {
        text: ''
      },
      labels: {
        style: {
          color: 'white'
        }
      }
    },
    yAxis: {
      title: {
        text: ''
      },
      labels: {
        style: {
          color: 'white'
        }
      }
    },
    series: [{
      type: 'line',
      name: 'Regression Line',
      color: 'rgba(255, 0, 0, 1)',
      data: []
    },{
      type: 'scatter',
      name: 'Scatter Plot',
      color: 'rgba(239, 108, 0, .3)',
      marker: {
        radius: 2
      },
      crisp: true,
      data: []
    }] as any,
    legend: {
      itemStyle: {
        color: 'white'
      }
    }
  };

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);

    for(const propName in changes){
      const change = changes[propName];
      console.log(change.currentValue);

      if(change.currentValue == null)
        return;

      this.data = change.currentValue;

      this.data.info.loss = Math.round(this.data.info.loss * 100) / 100;

      this.transform();

      console.log(this.data);
    }
  }

  transform(){
    console.log(this.data);

    let lineArray: number[][] = [];
    let scatterArray: number[][] = [];

    this.data.line.series.forEach(el => {
      lineArray.push([el.name, el.value])
    })
    this.data.scatter.series.forEach(el => {
      scatterArray.push([el.x, el.y])
    })

    this.chartOptions.xAxis.title.text = localStorage.getItem('graphXAxis');
    this.chartOptions.yAxis.title.text = localStorage.getItem('graphYAxis');

    console.log(lineArray);
    console.log(scatterArray);

    this.chartOptions.series[0].data = lineArray;
    this.chartOptions.series[1].data = scatterArray;
  }
}
