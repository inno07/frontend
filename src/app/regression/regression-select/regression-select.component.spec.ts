import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegressionSelectComponent } from './regression-select.component';

describe('RegressionSelectComponent', () => {
  let component: RegressionSelectComponent;
  let fixture: ComponentFixture<RegressionSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegressionSelectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegressionSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
