import {Component, EventEmitter, Input, Output} from '@angular/core';
import {DataService} from "../../services/data.service";
import {FetchResponse} from "../../responses/fetch-response";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {GraphResponse} from "../../responses/graph-response";
import {GraphRequest} from "../../requests/graph-request";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-regression-select',
  templateUrl: './regression-select.component.html',
  styleUrls: ['./regression-select.component.scss']
})
export class RegressionSelectComponent {

  isLoading: boolean = false;

  @Input() regressionSelectInput: FetchResponse = { title: '', headers: []};

  @Output() regressionSelectOutput = new EventEmitter<GraphResponse>();

  formGroup = new FormGroup({
    xAxis: new FormControl('', Validators.required),
    yAxis: new FormControl('', Validators.required)
  });

  constructor(private dataService: DataService, private snackbar: MatSnackBar) { }

  submit(){
    console.log(this.formGroup);

    if(this.formGroup.invalid)
      return;

    localStorage.setItem('graphXAxis', this.formGroup.value.xAxis);
    localStorage.setItem('graphYAxis', this.formGroup.value.yAxis);

    const graphRequest: GraphRequest = {
      model: 'regression',
      data: this.regressionSelectInput.title,
      xAxis: this.formGroup.value.xAxis,
      yAxis: this.formGroup.value.yAxis,
      ts: new Date()
    };

    console.log(graphRequest);

    this.isLoading = true;

    this.dataService.graph(graphRequest).subscribe({
      next: res => {
        console.log(res);
        this.isLoading = false;
        this.regressionSelectOutput.emit(res);
      },
      error: err => {
        console.log(err);
        this.isLoading = false;
        this.snackbar.open("Internal server error", '', { duration: 3000, panelClass: ['error-snackbar'] });
      }
    })
  }
}
