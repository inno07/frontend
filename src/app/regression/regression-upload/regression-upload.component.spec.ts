import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegressionUploadComponent } from './regression-upload.component';

describe('UploadComponent', () => {
  let component: RegressionUploadComponent;
  let fixture: ComponentFixture<RegressionUploadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegressionUploadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegressionUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
