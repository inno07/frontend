import {Component, EventEmitter, Output} from '@angular/core';
import {RegressionRequest} from "../../requests/regression-request";
import {DataService} from "../../services/data.service";
import {FetchResponse} from "../../responses/fetch-response";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-regression-upload',
  templateUrl: './regression-upload.component.html',
  styleUrls: ['./regression-upload.component.scss']
})
export class RegressionUploadComponent {

  constructor(private dataService: DataService, private snackbar: MatSnackBar) { }

  @Output() regressionUploadOutput = new EventEmitter<FetchResponse>();

  upload($event: any){
    if($event.target == null)
      return;

    const file: File = $event.target.files[0];

    const formData = new FormData();
    formData.append("userID", "0123456789");
    formData.append("file", file);
    formData.append("title", file.name);

    const uploadRequest: RegressionRequest = {
      formData: formData,
      ts: new Date()
    }

    this.dataService.regression(uploadRequest).subscribe(res => {
      console.log(res);
      this.regressionUploadOutput.emit(res);
    }, err => {
      console.log(err);
      this.snackbar.open("Internal server error", '', { duration: 3000, panelClass: ['error-snackbar'] });
    });
  }
}
