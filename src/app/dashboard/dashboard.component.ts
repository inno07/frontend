import { Component } from '@angular/core';
import {FetchResponse} from "../responses/fetch-response";
import {GraphResponse} from "../responses/graph-response";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

  title = 'Dashboard';
  listActive: FetchResponse | null = null;
  graphActive: GraphResponse | null = null;
  graphLoading: boolean = false;

  graphCard = {
    id: 1,
    title: "Graph",
    cols: 2,
    rows: 1
  }

  listCard = {
    id: 2,
    title: "List",
    cols: 1,
    rows: 1
  }

  optionsCard = {
    id: 3,
    title: "Options",
    cols: 1,
    rows: 1
  }

  getListActive($event: any){
    this.listActive = $event;
  }

  getGraphActive($event: any){
    this.graphActive = $event;
  }

  getGraphLoading($event: any){
    this.graphLoading = $event;
  }

  constructor() {}
}
