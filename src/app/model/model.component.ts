import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.scss']
})
export class ModelComponent {

  constructor() { }

  @Output() modelOutput = new EventEmitter<String>();

  regression(){
    console.log("regression");
    this.modelOutput.emit("regression");
  }

  classification(){
    console.log("classification");
    this.modelOutput.emit("classification");
  }

}
