import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassificationUploadComponent } from './classification-upload.component';

describe('ClassificationParamSelectComponent', () => {
  let component: ClassificationUploadComponent;
  let fixture: ComponentFixture<ClassificationUploadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClassificationUploadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassificationUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
