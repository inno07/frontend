import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ClassificationLimitRequest} from "../../requests/classification-limit-request";
import {DataService} from "../../services/data.service";
import {ClassificationNoLimitRequest} from "../../requests/classification-no-limit-request";
import {ClassificationResponse} from "../../responses/classification-response";
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-classification-upload',
  templateUrl: './classification-upload.component.html',
  styleUrls: ['./classification-upload.component.scss']
})
export class ClassificationUploadComponent {
  @Output() classificationUploadOutput = new EventEmitter<ClassificationResponse>();

  selectDependentColumnsFormGroup = new FormGroup({
    dependentCols: new FormControl('', Validators.required)
  });

  selectPredictColumnFormGroup = new FormGroup({
    predictCol: new FormControl('', Validators.required)
  });

  selectLimitFormGroup = new FormGroup({
    limit: new FormControl('', Validators.required)
  })

  hasLimit: boolean = false;
  selectClassificationActive = true;
  selectTrainFileActive = false;
  selectDependentColumnsActive = false;
  selectPredictColumnActive = false;
  selectLimitActive = false;
  selectDataFileActive = false;

  isLoading: boolean = false;

  headers: string[] = [];

  remainingHeaders: string[] = [];

  formData: FormData = new FormData();
  dependentCols: string[] = [];
  predictCol: string = "";
  limitValue: number = 0;

  constructor(private dataService: DataService, private router: Router, private snackbar: MatSnackBar) { }

  limit(){
    this.hasLimit = true;
    this.selectClassificationActive = false;
    this.selectTrainFileActive = true;
  }

  noLimit(){
    this.hasLimit = false;
    this.selectClassificationActive = false;
    this.selectTrainFileActive = true;
  }

  selectTrainFile($event: any){
    this.formData.delete("trainFile");
    this.headers = [];

    if($event.target == null)
      return;

    const trainFile: File = $event.target.files[0];

    this.formData.append("trainFile", trainFile);

    const reader: FileReader = new FileReader();
    reader.readAsText(trainFile);
    reader.onload = () => {
      const res = reader.result as string;
      const lines = res.split('\n');
      const data = lines[0].split(',');

      data.forEach(e => {
        this.headers.push(e);
      })
    };

    this.selectTrainFileActive = false;
    this.selectDependentColumnsActive = true;
  }

  selectDependentColumns(){
    console.log(this.selectDependentColumnsFormGroup.value.dependentCols);

    this.remainingHeaders = this.headers;

    this.dependentCols = this.selectDependentColumnsFormGroup.value.dependentCols;

    this.remainingHeaders = this.headers.filter(x => !this.dependentCols.includes(x));

    this.selectDependentColumnsActive = false;
    this.selectPredictColumnActive = true;
  }

  selectPredictColumn(){

    this.predictCol = this.selectPredictColumnFormGroup.value.predictCol;

    this.selectPredictColumnActive = false;

    if(this.hasLimit)
      this.selectLimitActive = true;
    else
      this.selectDataFileActive = true;
  }

  selectLimit(){
    this.limitValue = this.selectLimitFormGroup.value.limit;

    this.selectLimitActive = false;
    this.selectDataFileActive = true;
  }

  selectDataFile($event: any){
    this.formData.delete("dataFile");

    if($event.target == null)
      return;

    const dataFile: File = $event.target.files[0];

    this.formData.append("dataFile", dataFile);

    if(this.hasLimit)
      this.createLimitRequest();
    else
      this.createNoLimitRequest();
  }

  createLimitRequest(){

    const request: ClassificationLimitRequest = {
      formData: this.formData,
      dependentCols: this.dependentCols,
      predictCol: this.predictCol,
      limit: this.limitValue
    }

    this.selectDataFileActive = false;
    this.isLoading = true;

    console.log(request);

    this.dataService.classificationLimit(request).subscribe(res => {
      console.log(res);
      this.classificationUploadOutput.emit(res);
    }, err => {
      console.log(err);
      this.isLoading = false;
      this.selectDataFileActive = true;
      this.snackbar.open("Internal server error", '', { duration: 3000, panelClass: ['error-snackbar'] });
    });
  }

  createNoLimitRequest(){
    console.log(this.limitValue);

    const request: ClassificationNoLimitRequest = {
      formData: this.formData,
      dependentCols: this.dependentCols,
      predictCol: this.predictCol
    }

    this.selectDataFileActive = false;
    this.isLoading = true;

    console.log(request);

    this.dataService.classificationNoLimit(request).subscribe(res => {
      console.log(res);
      this.classificationUploadOutput.emit(res);
    }, err => {
      console.log(err);
      this.isLoading = false;
      this.selectDataFileActive = true;
      this.snackbar.open("Internal server error", '', { duration: 3000, panelClass: ['error-snackbar'] });
    });
  }

  back(){
    if(this.selectClassificationActive){
      this.router.navigate(['/dashboard']);
    }

    if(this.selectTrainFileActive){
      this.selectTrainFileActive = false;
      this.selectClassificationActive = true;
    }

    if(this.selectDependentColumnsActive){
      this.selectDependentColumnsActive = false;
      this.selectTrainFileActive = true;
      return;
    }

    if(this.selectPredictColumnActive){
      this.selectPredictColumnActive = false;
      this.selectDependentColumnsActive = true;
      return;
    }

    if(this.selectLimitActive){
      this.selectLimitActive = false;
      this.selectPredictColumnActive = true;
    }

    if(this.selectDataFileActive){
      this.selectDataFileActive = false;

      if(this.hasLimit)
        this.selectLimitActive = true;

      if(!this.hasLimit)
        this.selectPredictColumnActive = true;

      return;
    }
  }

}
