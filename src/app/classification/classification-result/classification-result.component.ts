import {Component, Input, OnInit} from '@angular/core';
import {ClassificationResponse} from "../../responses/classification-response";

@Component({
  selector: 'app-classification-result',
  templateUrl: './classification-result.component.html',
  styleUrls: ['./classification-result.component.scss']
})
export class ClassificationResultComponent implements OnInit {

  @Input() classificationResultInput: ClassificationResponse = {
    head: [],
    toPredict: '',
    data: []
  };

  constructor() { }

  displayedColumns: string[] = new Array();

  dataSource: any[] = new Array();

  headers: string[] = new Array();

  ngOnInit(): void {

    console.log(this.classificationResultInput.head);

    this.classificationResultInput.head.forEach( e => {
      this.headers.push(e);
      this.displayedColumns.push(e);
    })
    this.headers.push('Percentage');
    this.displayedColumns.push('Percentage');

    this.classificationResultInput.data.forEach( e => {
      let entry: any[] = new Array();
      e.colValues.forEach( f => {
        entry.push(f);
      });
      entry.push(Math.round(((e.percentage * 100) / 100)).toString() + '%');

      this.dataSource.push(entry);
    })

    console.log(this.dataSource);
  }

}
