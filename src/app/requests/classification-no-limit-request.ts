export interface ClassificationNoLimitRequest {
  formData: FormData,
  dependentCols: string[],
  predictCol: string
}
