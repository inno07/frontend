export interface ClassificationLimitRequest {
  formData: FormData;
  dependentCols: string[];
  predictCol: string;

  limit: number;
}
