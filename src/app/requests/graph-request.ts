export interface GraphRequest {
  model: string;
  data: string;
  xAxis: string;
  yAxis: string;
  ts: Date;
}
