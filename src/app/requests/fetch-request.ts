export interface FetchRequest {
  jwt: string;
  userID: number;
  ts: Date;
}
