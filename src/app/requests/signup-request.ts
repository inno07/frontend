export interface SignupRequest {
  email: string;
  password: string;
  confirmPassword: string;
  username: string;
  ts: Date;
}
