export interface RegressionRequest {
  formData: FormData;
  ts: Date;
}
