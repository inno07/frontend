export const environment = {
  production: true,
  apiGatewayURLGo: 'http://userservice:8000/api',
  apiGatewayURLFlask: 'http://flask:5000/api'
};
